local Entity = require 'class' ()

function Entity:_init(name, number, comps)
  local config = love.filesystem.load("entity/" .. name .. ".lua")()
  self.number = number
  self.comps = {}

  for prop, _ in pairs(config) do
    self.comps[prop] = comps[prop](config)
  end
end

function Entity:update(dt, entities)
  for _, comp in pairs(self.comps) do
    comp:update(self, dt, entities)
  end
end

function Entity:draw(dt)
  if (self.comps.position) then
    for _, comp in pairs(self.comps) do
      comp:render(self)
    end
  end
end

return Entity
