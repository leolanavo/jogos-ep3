local Vec = require 'common/vec'

return {
  position = {
  },
  movement = {
    motion = Vec(2,2)
  },
  body = {
    size = 24
  },
  charge = {
   strength = -1
  },
}