local Vec = require 'common/vec'

return {
  position = {
    point = Vec(50, 50)
  },
  movement = {
    motion = Vec(-10, -5)
  },
  body = {
    size = 24
  }
}
