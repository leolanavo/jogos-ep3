local Vec = require 'common/vec'

return {
  position = {
    point = Vec(50, 50)
  },
  body = {
    size = 24
  },
  field = {
   strength = 1
  },
}