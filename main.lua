local Entity = require 'entity'
local Position = require 'component/position'
local Movement = require 'component/movement'
local Body = require 'component/body'
local Control = require 'component/control'
local Field = require 'component/field'
local Charge = require 'component/charge'

local width, height
local entities = {}

local compTable = {
  position = Position,
  movement = Movement,
  control = Control,
  body = Body,
  field = Field,
  charge = Charge,
}

function love.load()
  local entitiesFile = love.filesystem.load("scene/" .. arg[2] .. ".lua")()
  local index = 1
  for _, ent in pairs(entitiesFile) do
    for _ = 1, ent.n, 1 do
      entities[index] = Entity(ent.entity, index, compTable)
      index = index + 1
    end
  end

  love.window.setMode(2000, 1000)
  width, height = love.graphics.getDimensions()
end

function love.update(dt)
  for _, entity in pairs(entities) do
    entity:update(dt, entities)
  end
end

function love.draw()
  love.graphics.translate(width / 2, height / 2)

  for _, entity in pairs(entities) do
    if (entity.comps.control) then
      local x, y = entity.comps.position:getCoords()
      love.graphics.translate(-x, -y)
    end
  end
  love.graphics.setColor(1, 1, 1)
  love.graphics.circle("line", 0, 0, 1000)

  for _, entity in pairs(entities) do
    entity:draw()
  end
end