return {
  { entity = 'strongneg', n = 2 },
  { entity = 'strongpos', n = 3 },
  { entity = 'simplepos', n = 10 },
  { entity = 'slowpos', n = 5 },
  { entity = 'player', n = 1 },
  { entity = 'simpleneg', n = 10 },
  { entity = 'large', n = 3 },
}

