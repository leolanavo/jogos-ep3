local Charge = require 'class' ()

function Charge:_init(config)
  if (not config.charge.strength) then
    self.strength = 1
  else
    self.strength = config.charge.strength
  end
  self.rotation = math.sqrt(math.abs(self.strength))
  self.dtCicle = 0
end

function Charge:get()
  return self.strength
end

function Charge:update(_, dt, _)
  self.dtCicle = self.dtCicle + dt
  if(self.dtCicle >= 1) then
    self.dtCicle = 0
  end
end

function Charge:applyForce(entity, dt, force)
  local mass = 1
  local maxSpeed = math.huge

  if (entity.comps.body) then
    mass = entity.comps.body:get()
  end

  if (entity.comps.control) then
    maxSpeed = entity.comps.control:getMaxSpeed()
  end

  local dv = force * dt / mass
  entity.comps.movement:accelerate(dv, maxSpeed)

  return self
end

function Charge:setColor()
  if (self.strength > 0) then
    love.graphics.setColor(1, 0, 0, 0.8)
  elseif (self.strength < 0) then
    love.graphics.setColor(0, 0, 1, 0.8)
  else
    love.graphics.setColor(0, 0, 1, 0.8)
  end
end

function Charge:render(entity, _)
  self:setColor()
  local ex, ey = entity.comps.position:getCoords()
  local angle = 2 * math.pi * self.dtCicle * self.rotation

  local x = ex + 8 * math.cos(angle)
  local y = ey + 8 * math.sin(angle)
  love.graphics.circle("line", x, y, 4)

  x = ex - 8 * math.cos(angle)
  y = ey - 8 * math.sin(angle)
  love.graphics.circle("line", x, y, 4)
end

return Charge