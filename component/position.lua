local Position = require 'class' ()
local Vec = require 'common/vec'
local infinity = 10000

local function randomPosition()
  local angle = math.random() * 2 * math.pi
  local r = 1000 * math.sqrt(math.random())

  return r * math.cos(angle), r * math.sin(angle)
end

function Position:_init(config)
  if (not config.position.point) then
    self.position = Vec(randomPosition())
  else
    self.position = config.position.point
  end
end

function Position:get()
  return self.position
end

function Position:getCoords()
  return self.position:get()
end

function Position:invert(size)
  local v = self.position:normalized() * (1000 - size)
  local x, y = v:get()
  self.position:set(-x, -y)
end

function Position:move(ds)
  self.position = self.position + ds
end

function Position:dist(other)
  if (other) then
    return self.position:dist(other:get())
  else
    return infinity
  end
end

function Position:update(_, _, _)
  return self
end

function Position:render(entity)

  love.graphics.setColor(0, 255, 0)
  local comps = entity.comps
  local x, y = self:getCoords()
  local size = 8

  if (not comps.body) then
    if (not comps.charge and not comps.field) then
      love.graphics.circle("line", x, y, 8)
    end
  else
    size = comps.body:get()
  end

  local dist = self.position:dist(Vec(0, 0))
  if (dist + size > 1000) then
    self:invert(size)
  end
end

return Position
