local Field = require 'class' ()
local C = 1000

function Field:_init(config)
  if (not config.field.strength) then
    self.strength = 1
  else
    self.strength = config.field.strength
  end
end

function Field:update(entity, dt, entityList)
  for _, ent in pairs(entityList) do
    if (ent.comps.movement and ent.comps.charge and ent ~= entity) then
      local force = self:force(
        ent.comps.charge,
        ent.comps.position,
        entity.comps.position
      )

      ent.comps.charge:applyForce(ent, dt, force)
    end
  end
end

function Field:force(charge, chargePos, fieldPos)
  local dist = chargePos:get() - fieldPos:get()
  local f = (dist * C * self.strength * charge:get()) / dist:dot(dist)
  return f
end

function Field:setColor()
  if (self.strength > 0) then
    love.graphics.setColor(1, 0, 0, 0.8)
  elseif (self.strength < 0) then
    love.graphics.setColor(0, 0, 1, 0.8)
  else
    love.graphics.setColor(0, 1, 0, 0.8)
  end
end

function Field:render(entity)
  self:setColor()
  local x, y = entity.comps.position:getCoords()
  love.graphics.circle("line", x, y, math.abs(self.strength))
end

return Field