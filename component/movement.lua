local Movement = require 'class' ()
local Vec = require 'common/vec'

function Movement:_init(config)
  if (not config.movement.motion) then
    self.movement = Vec(0, 0)
  else
    self.movement = config.movement.motion
  end
end

function Movement:get()
  return self.movement
end

function Movement:accelerate(dv, maxSpeed)
  self.movement = self.movement + dv
  self.movement:clamp(maxSpeed)
end

function Movement:update(entity, dt, entityList)
  local ds = self.movement * dt
  entity.comps.position:move(ds)

  for _, ent in pairs(entityList) do
    if (ent ~= entity and ent.comps.body) then
      local thisSize = 8
      if (entity.comps.body ~= nil) then
        thisSize = entity.comps.body:get()
      end
      local hasCollided = ent.comps.body:collided(
        ent.comps.position:dist(entity.comps.position),
        thisSize
      )
      if (hasCollided) then
        local deltaX = entity.comps.position:get() - ent.comps.position:get()
        local l =
          thisSize + ent.comps.body:get() - deltaX:length()

        self:dealWithCollision(entity.comps.position, deltaX, l)
      end
    end
  end
end

function Movement:dealWithCollision(position, deltaX, l)
  local direction = deltaX / deltaX:length()
  position:move(direction * l)
  self.movement =
    self.movement - direction * direction:dot(self.movement)
end

function Movement:render(_)
  return self
end

return Movement