local Control = require 'class' ()

local Vec = require 'common/vec'

function Control:_init(config)
  if (not config.control.max_speed or not config.control.acceleration) then
    self.acceleration = 0.0
    self.maxSpeed = 50.0
  else
    self.acceleration = config.control.acceleration
    self.maxSpeed = config.control.max_speed
  end

  self.accelerationVecs = {
    down = Vec(0, self.acceleration),
    up = Vec(0, -self.acceleration),
    right = Vec(self.acceleration, 0),
    left = Vec(-self.acceleration, 0),
  }
end

function Control:getMaxSpeed()
  return self.maxSpeed
end

function Control:update(entity, dt, _)
  local acceleration = Vec(0, 0)

  for key, vec in pairs(self.accelerationVecs) do
    if (love.keyboard.isDown(key)) then
      acceleration = acceleration + vec
    end
  end

  local dv = acceleration * dt
  entity.comps.movement:accelerate(dv, self.maxSpeed)
end

-- down: (0, 2ts, -tS, -tS, tS, -tS)
-- up: (0, -2ts, -tS, tS, tS, tS)
-- right: (2ts, 0, -ts, ts, -ts, -ts)
-- left: (-2ts, 0, ts, ts, ts, -ts)

function Control:render(entity)
  local x, y = entity.comps.position:getCoords()
  local tS = 3

  if (entity.comps.body) then
    tS = entity.comps.body:get() / 3
  end

  love.graphics.setColor(255, 255, 255)
  love.graphics.polygon("fill", x - tS - tS, y, x + tS, y + tS, x + tS, y - tS)

  return self
end

return Control