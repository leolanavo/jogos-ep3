local Body = require 'class' ()

function Body:_init(config)
  if (not config.body) then
    self.size = 8
  else
    self.size = config.body.size
  end
end

function Body:get()
  return self.size
end

function Body:collided(dist, otherSize)
  return dist < self.size + otherSize
end

function Body:update(_, _, _)
  return self
end

function Body:render(entity)
  love.graphics.setColor(0, 1, 0)
  local x, y = entity.comps.position:getCoords()

  if (entity.comps.field) then
    entity.comps.field:setColor()
  end

  love.graphics.circle("fill", x, y, entity.comps.body:get())

  return self
end

return Body